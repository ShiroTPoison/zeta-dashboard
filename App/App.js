import "react-native-gesture-handler";
import "expo-dev-client";
import { enableFreeze } from "react-native-screens";
import React from "react";
import { Provider as StoreProvider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import i18n from "i18n-js";
import { useKeepAwake } from "expo-keep-awake";
// import { setStatusBarStyle } from "expo-status-bar";

import configureStore from "./src/redux/store";
import AppNavigator from "./src/navigator/navigation";
import { languageDictionary } from "./assets/locale/index";

i18n.translations = languageDictionary.languageSet;
i18n.fallbacks = true;
enableFreeze(true);
// setStatusBarStyle("light");

export default function App() {
  try {
    useKeepAwake();
  } catch (e) {
    console.error(e);
  }

  return (
    <StoreProvider store={configureStore().store}>
      <PersistGate loading={null} persistor={configureStore().persistor}>
        {/* <StatusBar style="auto" /> */}
        <AppNavigator />
      </PersistGate>
    </StoreProvider>
  );
}
