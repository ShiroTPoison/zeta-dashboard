import { createStore, applyMiddleware, compose } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import AsyncStorage from "@react-native-async-storage/async-storage";
import thunk from "redux-thunk";
import rootReducers from "./reducer";

import {
  saveOnLogin,
  ServiceMiddleware,
} from "./middleware/serviceMiddleWare.js";

//ConfigForPersist
const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  blacklist: ["quarterReducer", "fYearReducer"],
};

//Reducer
const persistedReducer = persistReducer(persistConfig, rootReducers);
//MiddleWares
let middleware = [thunk, ServiceMiddleware, saveOnLogin];

export default function configureStore() {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  let store = createStore(
    persistedReducer,
    composeEnhancers(applyMiddleware(...middleware))
  );
  let persistor = persistStore(store);

  return { store, persistor };
}
