import { combineReducers } from "redux";
import commonReducers from "../common/reducers";
import loginReducers from "../../screens/login/reducers";
import dashboardReducers from "../../screens/dashboard/reducers";
import settingReducers from "../../screens/setting/reducers";

const rootReducer = (state, action) => {
  if (action.type === "RESET_ACTION") {
    //Reseting Redux Store ( LogOut )
    return appReducer(undefined, action);
  }

  return appReducer(state, action);
};

const appReducer = combineReducers({
  ...commonReducers,
  ...loginReducers,
  ...dashboardReducers,
  ...settingReducers,
});

export default rootReducer;
