import * as types from "../constants/constants";

export function logout() {
  return async (dispatch) => {
    dispatch({ type: types.RESET_ACTION });
  };
}

export function setAppAppearance(value) {
  return async (dispatch) => {
    dispatch({ type: types.APP_APPEARANCE, payload: value });
  };
}

export function setLocation(value) {
  return async (dispatch) => {
    dispatch({ type: types.LOCATION, payload: value });
  };
}

export function setAddress(value) {
  return async (dispatch) => {
    dispatch({ type: types.ADDRESS, payload: value });
  };
}

export function setLanguage(value) {
  return async (dispatch) => {
    dispatch({ type: types.LANGUAGE, payload: value });
  };
}

export function setGDate(value) {
  return async (dispatch) => {
    dispatch({ type: types.G_DATE, payload: value });
  };
}

export function setGMonth(value) {
  return async (dispatch) => {
    dispatch({ type: types.G_MONTH, payload: value });
  };
}

export function setDateFormat(value) {
  return async (dispatch) => {
    dispatch({ type: types.DATE_FORMAT, payload: value });
  };
}

export function setCompanyId(value) {
  return async (dispatch) => {
    dispatch({ type: types.COMPANY_ID, payload: value });
    return value;
  };
}

export function setCompanyName(value) {
  return async (dispatch) => {
    dispatch({ type: types.COMPANY_NAME, payload: value });
    return value;
  };
}

export function setQuarter(value) {
  return async (dispatch) => {
    dispatch({ type: types.QUARTER, payload: value });
    return value;
  };
}

export function setFYear(value) {
  return async (dispatch) => {
    dispatch({ type: types.F_YEAR, payload: value });
    return value;
  };
}
