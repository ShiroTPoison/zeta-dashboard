import appAppearanceReducer from "./setAppApearanceReducer";
import locationReducer from "./locationReducer";
import addressReducer from "./addressReducer";
import languageReducer from "./languageReducer";
import gDateReducer from "./globalDateReducer";
import gMonthReducer from "./globalMonthReducer";
import dateFormatReducer from "./dateFormatReducer";
import companyIdReducer from "./companyIdReducer";
import companyNameReducer from "./companyNameReducer";
import quarterReducer from "./quarterReducer";
import fYearReducer from "./fYearReducer";

const reducers = {
  appAppearanceReducer,
  addressReducer,
  locationReducer,
  languageReducer,
  gDateReducer,
  gMonthReducer,
  dateFormatReducer,
  companyIdReducer,
  companyNameReducer,
  quarterReducer,
  fYearReducer,
};
export default reducers;
