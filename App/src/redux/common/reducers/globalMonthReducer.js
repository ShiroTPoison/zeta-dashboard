import * as types from "../constants/constants";

let initial = {
  loading: false,
  data: new Date(),
  error: null,
};
export default function (state = initial, action) {
  switch (action.type) {
    case types.G_MONTH:
      return { ...state, loading: false, data: action.payload, error: null };
    default:
      return state;
  }
}
