import * as types from "../constants/constants";

let initial = {
  loading: false,
  data: "yyyy/MM/dd",
  error: null,
};
export default function (state = initial, action) {
  switch (action.type) {
    case types.DATE_FORMAT:
      return { ...state, loading: false, data: action.payload, error: null };
    default:
      return state;
  }
}
