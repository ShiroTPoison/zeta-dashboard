export const RESET_ACTION = "RESET_ACTION";

export const APP_APPEARANCE = "APP_APPEARANCE";

export const LOCATION = "LOCATION";

export const ADDRESS = "ADDRESS";

export const LANGUAGE = "LANGUAGE";

export const G_DATE = "G_DATE";

export const G_MONTH = "G_MONTH";

export const DATE_FORMAT = "DATE_FORMAT";

export const COMPANY_ID = "COMPANY_ID";

export const COMPANY_NAME = "COMPANY_NAME";

export const QUARTER = "QUARTER";

export const F_YEAR = "F_YEAR";
