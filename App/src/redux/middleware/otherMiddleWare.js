import axios from "axios";
import * as commonTypes from "../common/constants/constants";

//setAuthToken
export function setToken(token) {
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
}

//setCompanyId
export function setCompanyId(dispatch, data) {
  dispatch({
    type: commonTypes.COMPANY_ID,
    payload: data,
  });
}

//SetCompanyName
export function setCompanyName(dispatch, data) {
  dispatch({
    type: commonTypes.COMPANY_NAME,
    payload: data,
  });
}

//SetDateFormat
export function setDateFormat(dispatch, data) {
  dispatch({
    type: commonTypes.DATE_FORMAT,
    payload: data,
  });
}

//SetYear
export function setFYear(dispatch, data) {
  dispatch({
    type: commonTypes.F_YEAR,
    payload: data,
  });
}
