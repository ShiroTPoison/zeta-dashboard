/* eslint-disable no-unused-vars */
import axios from "axios";
import {
  setFYear,
  setToken,
  setCompanyId,
  setDateFormat,
  setCompanyName,
} from "./otherMiddleWare";

//MiddlewaresForLoginSuccess
export const saveOnLogin = (store) => (next) => async (action) => {
  if (action.type === "LOGIN_ACCOUNT_SUCCESS") {
    const { dispatch, getState } = store;
    setToken(action.payload.access_token);
    setDateFormat(dispatch, action.payload?.session?.dateformat);

    //Setting company Id
    if (
      !getState().companyIdReducer.data ||
      getState().companyIdReducer.data === null ||
      getState().companyIdReducer.data === undefined
    ) {
      setCompanyId(dispatch, action.payload?.session?.companyid);
    }

    // Setting Copmany Name
    if (
      !getState().companyNameReducer.data ||
      getState().companyNameReducer.data === null ||
      getState().companyNameReducer.data === undefined
    ) {
      setCompanyName(dispatch, action.payload?.session?.companyname);
    }

    //Setting Year
    setFYear(dispatch, action.payload?.session?.financialyearid);
  }

  return next(action);
};

//ServiceMiddleware
export const ServiceMiddleware = (store) => (next) => async (action) => {
  if (action === undefined || !action.serviceName) {
    if (action === undefined) action = { type: "" };
    next(action);
    return;
  }

  const { serviceName, method, headers, onLoad, onSuccess, onFailure, data } =
    action;
  const { dispatch } = store;

  setBySentType(onLoad, dispatch);

  try {
    let response = await axios({
      url: serviceName,
      method: method,
      headers: headers,
      data: data,
    });

    setBySentType(onSuccess, dispatch, response.data);
  } catch (error) {
    setBySentType(onFailure, dispatch, error);
  }
};

const setBySentType = (callBack, dispatch, data = {}) => {
  if (typeof callBack === "function") callBack(data, dispatch);
  else if (!(callBack instanceof Array) && typeof callBack === "object") {
    if (callBack["payload"] !== undefined)
      callBack["payload"] = Object.assign({}, data);
    dispatch(callBack);
  } else if (callBack instanceof Array && typeof callBack === "object") {
    callBack.forEach((ele) => {
      dispatch(ele);
    });
  }
};
