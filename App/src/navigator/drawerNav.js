import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Dimensions, Image, StyleSheet, View } from "react-native";
import { useTheme } from "react-native-paper";
import { connect } from "react-redux";
import { format } from "date-fns";

import { logout } from "../redux/common/actions/actions";
import { icons } from "../../assets/images";
import { drawerActiveTint, drawerIcon } from "../styles/navCss";
import { IonIcons } from "../constants";
import { CustomCaption } from "../components/customText";
import { mgHs } from "../styles";
import DrawerContent from "../components/drawer";

//screens
import Dashboard from "../screens/dashboard";
import BankDetails from "../screens/bankDetails";
import ExpenseDetails from "../screens/expenseDetails";
import TopSales from "../screens/topSales";
import Setting from "../screens/setting";
// eslint-disable-next-line no-unused-vars
import { Playground } from "../screens/playground";

const Drawer = createDrawerNavigator();

const DrawerNav = (props) => {
  const { colors } = useTheme();
  const style = styles(colors);
  const DrawerIcons = ({ size, focused, icon }) => (
    <Image
      source={icon}
      style={[focused ? null : null, { height: size, width: size }]}
    />
  );

  const headerRight = () => (
    <View>
      <CustomCaption style={style.time}>
        {format(new Date(), "EEEE, MMMM")}
      </CustomCaption>
      <CustomCaption style={style.time}>
        {format(new Date(), "d, yyy")}
      </CustomCaption>
    </View>
  );

  return (
    <Drawer.Navigator
      screenOptions={{
        // swipeEnabled: false,
        drawerActiveBackgroundColor: colors.primary,
        drawerActiveTintColor: drawerActiveTint,
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: colors.notification },
        headerTintColor: drawerActiveTint,
        drawerStyle: style.drawer,
        drawerItemStyle: style.drawerItem,
        drawerIcon: ({ color, size }) => (
          <IonIcons name={drawerIcon} size={size} color={color} />
        ),
      }}
      drawerContent={(dCprops) => (
        <DrawerContent
          {...dCprops}
          logout={props.logout}
          submitLoginReducer={props.submitLoginReducer.data}
          profileUrl={props.submitLoginReducer.data?.user_pp}
          drawerItemStyle={style.drawerItem}
        />
      )}
    >
      <Drawer.Screen
        name="dashboard"
        component={Dashboard}
        options={{
          title: "Dashboard",
          headerTitleContainerStyle: { height: 0, width: 0 },
          headerRight: headerRight,

          drawerIcon: ({ color, focused, size }) => (
            <DrawerIcons
              color={color}
              focused={focused}
              size={size}
              icon={icons.drawer.report}
            />
          ),
        }}
      />

      <Drawer.Screen
        name="bankDetails"
        component={BankDetails}
        options={{
          title: "BankDetails",
          drawerIcon: ({ color, focused, size }) => (
            <DrawerIcons
              color={color}
              focused={focused}
              size={size}
              icon={icons.drawer.bank}
            />
          ),
        }}
      />

      <Drawer.Screen
        name="expenseDetails"
        component={ExpenseDetails}
        options={{
          title: "ExpenseDetails",
          drawerIcon: ({ color, focused, size }) => (
            <DrawerIcons
              color={color}
              focused={focused}
              size={size}
              icon={icons.drawer.expense}
            />
          ),
        }}
      />

      <Drawer.Screen
        name="topSales"
        component={TopSales}
        options={{
          title: "TopSales",
          drawerIcon: ({ color, focused, size }) => (
            <DrawerIcons
              color={color}
              focused={focused}
              size={size}
              icon={icons.drawer.topSale}
            />
          ),
        }}
      />

      <Drawer.Screen
        name="setting"
        component={Setting}
        options={{
          title: "Setting",
          drawerIcon: ({ color, focused, size }) => (
            <IonIcons
              color={color}
              focused={focused}
              size={size}
              name={`settings-outline`}
            />
          ),
        }}
      />

      {/* <Drawer.Screen name="playground" component={Playground} /> */}
    </Drawer.Navigator>
  );
};

function mapStateToProps({ submitLoginReducer }) {
  return {
    submitLoginReducer,
  };
}
export default connect(mapStateToProps, {
  logout,
})(DrawerNav);

const styles = () =>
  // colors
  StyleSheet.create({
    drawerItem: {
      // borderWidth: StyleSheet.hairlineWidth,
    },

    drawer: {
      width: 0.8 * Dimensions.get("window").width,
    },

    time: {
      color: drawerActiveTint,
      textAlign: "right",
      marginHorizontal: mgHs,
    },
  });
