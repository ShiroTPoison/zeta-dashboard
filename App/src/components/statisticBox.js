import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import {
  Divider,
  Surface,
  TouchableRipple,
  useTheme,
} from "react-native-paper";
import Animated, {
  BounceInUp,
  BounceOutDown,
  Layout,
} from "react-native-reanimated";

import {
  bRs,
  bRss,
  mgHs,
  onBackgroundDark,
  pdHs,
  pdVs,
  pdVss,
} from "../styles";
import { CustomSquareButton } from "./buttons";
import { CustomSubheading, CustomText, CustomTitle } from "./customText";
import { GapH } from "./gap";

export const Statistics = ({
  style,
  bg,
  title,
  heading,
  current,
  prev,
  pr,
  payable,
  recievable,
}) => {
  const [next, setNext] = useState(false);
  const [data, setData] = useState(current);

  const { colors } = useTheme();
  const styleV = styles({ bg: bg, colors: colors });

  return (
    <Animated.View
      entering={BounceInUp}
      exiting={BounceOutDown}
      layout={Layout.springify()}
      style={styleV.container}
    >
      <Surface style={[styleV.box, style]}>
        <View style={styleV.row}>
          <CustomTitle style={styleV.title}>{title}</CustomTitle>

          {prev && (
            <View style={{ flexDirection: "row" }}>
              {!next ? (
                <CustomSquareButton
                  mode={"contained"}
                  title={`Previous`}
                  onPress={() => {
                    setData(prev);
                    setNext(false);
                  }}
                />
              ) : (
                <TouchableRipple
                  onPress={() => {
                    setData(prev);
                    setNext(false);
                  }}
                >
                  <CustomSubheading style={{ padding: 5 }}>
                    {`PREVIOUS`}
                  </CustomSubheading>
                </TouchableRipple>
              )}

              <GapH small={true} />
              {next ? (
                <CustomSquareButton
                  mode={"contained"}
                  title={`Current`}
                  onPress={() => {
                    setData(current);
                    setNext(true);
                  }}
                />
              ) : (
                <TouchableRipple
                  onPress={() => {
                    setData(current);
                    setNext(true);
                  }}
                >
                  <CustomSubheading style={{ padding: 5 }}>
                    {`CURRENT`}
                  </CustomSubheading>
                </TouchableRipple>
              )}
            </View>
          )}
        </View>

        {/* Heading */}
        <View style={styleV.headerRow}>
          {heading.map((hItem, i) => (
            <CustomSubheading key={Math.random() + i} style={styleV.heading}>
              {hItem}
            </CustomSubheading>
          ))}
        </View>

        {/* Data Rows */}
        {!pr ? (
          data.map((item, dIndex) => {
            return (
              <View key={dIndex + Math.random()}>
                <View style={styleV.row}>
                  <CustomText>{`Sale`}</CustomText>
                  <CustomText>{`${item?.netSale}`}</CustomText>
                </View>

                <View style={styleV.row}>
                  <CustomText>{`Purchase`}</CustomText>
                  <CustomText>{`${item?.netPurchase}`}</CustomText>
                </View>

                <Divider style={styleV.divider} />

                <View style={styleV.row}>
                  <CustomText>{`GP`}</CustomText>
                  <CustomText>{`${item?.netGP}`}</CustomText>
                </View>

                <View style={styleV.row}>
                  <CustomText>{`Expense`}</CustomText>
                  <CustomText>{`${item?.netExp}`}</CustomText>
                </View>

                <Divider style={styleV.divider} />

                <View style={styleV.row}>
                  <CustomText>{`NP`}</CustomText>
                  <CustomText>{`${item?.netProfit}`}</CustomText>
                </View>
              </View>
            );
          })
        ) : (
          // PAYABLE RECIEVABLE ROW
          <>
            <View style={styleV.row}>
              <CustomText>{`Payable`}</CustomText>
              <CustomText>{`${payable}`}</CustomText>
            </View>

            <Divider style={styleV.divider} />

            <View style={styleV.row}>
              <CustomText>{`Recievable`}</CustomText>
              <CustomText>{`${recievable}`}</CustomText>
            </View>
          </>
        )}
      </Surface>
    </Animated.View>
  );
};
const styles = ({ bg, colors }) =>
  StyleSheet.create({
    box: {
      backgroundColor: bg,
      paddingHorizontal: mgHs,
      paddingVertical: pdVs,
      borderRadius: bRs,
      borderTopWidth: bRs,
      borderColor: colors.notification,
      width: "100%",
    },

    row: {
      flexDirection: "row",
      justifyContent: "space-between",
      paddingVertical: pdVs,
      paddingHorizontal: pdHs,
    },

    headerRow: {
      flexDirection: "row",
      justifyContent: "space-between",
      backgroundColor: colors.primary,
      paddingVertical: pdVss,
      borderRadius: bRss,
      paddingHorizontal: pdHs,
    },

    heading: {
      color: onBackgroundDark,
    },

    title: {
      textDecorationLine: "underline",
    },

    divider: {
      backgroundColor: colors.text,
      height: StyleSheet.hairlineWidth,
      width: "100%",
      alignSelf: "center",
    },
  });
