import React, { useState } from "react";
import { View, StyleSheet, FlatList, RefreshControl } from "react-native";
import {
  Divider,
  Surface,
  TouchableRipple,
  useTheme,
} from "react-native-paper";
import Animated from "react-native-reanimated";

import {
  bRs,
  bRss,
  mgHs,
  onBackgroundDark,
  pdHs,
  pdVs,
  pdVss,
} from "../styles";
import { CustomSquareButton } from "./buttons";
import { CustomSubheading, CustomText, CustomTitle } from "./customText";
import { GapH, GapV } from "./gap";
import { windowWidth } from "../constants";

export const DetailsBox = ({
  style,
  bg,
  title,
  current,
  heading,
  bank,
  prev,
  expense,
  sales,
  refresh,
  refreshing,
  onRefresh,
}) => {
  const [next, setNext] = useState(true);
  const [data, setData] = useState(current);
  const [viewWidth, setViewWidth] = React.useState(0);
  const { colors } = useTheme();
  const styleV = styles({ bg: bg, colors: colors });

  const onLayout = (event) => {
    var { width } = event.nativeEvent.layout;
    setViewWidth(width);
  };

  const renderItem = ({ item, index }) => {
    return data.length - 1 != index ? (
      <View style={[styleV.container]}>
        <View style={styleV.row}>
          {/* Bank Details */}
          {bank && (
            <>
              <CustomText style={styleV.text}>{item?.bankCode}</CustomText>

              <CustomText style={styleV.text}>{`${item?.bankName}`}</CustomText>

              <CustomText
                style={[styleV.text, styleV.lastTextObj]}
              >{`${item?.bankBalance}`}</CustomText>
            </>
          )}

          {/* Expense Details */}
          {expense && (
            <>
              <CustomText style={styleV.text}>{item?.accountCode}</CustomText>

              <CustomText
                style={styleV.text}
              >{`${item?.accountName}`}</CustomText>

              <CustomText
                style={[styleV.text, styleV.lastTextObj]}
              >{`${item?.closingBal}`}</CustomText>
            </>
          )}

          {/* Expense Details */}
          {sales && (
            <>
              <CustomText style={[styleV.text, { width: windowWidth * 0.6 }]}>
                {`${item?.customerName}`}
              </CustomText>

              <CustomText
                style={[
                  styleV.text,
                  styleV.lastTextObj,
                  { width: viewWidth * 0.2 },
                ]}
              >{`${item?.customerSaleAmount}`}</CustomText>
            </>
          )}
        </View>

        <Divider style={styleV.divider} />
      </View>
    ) : (
      <View>
        {/* Bank Details */}
        {bank && (
          <View style={[styleV.row, styleV.lastRow]}>
            <CustomSubheading style={styleV.lastRowText}>
              {`Total`}
            </CustomSubheading>

            <CustomSubheading
              style={styleV.lastRowText}
            >{`${item?.bankBalance}`}</CustomSubheading>
          </View>
        )}

        {/* Expense Details */}
        {expense && (
          <View style={[styleV.row, styleV.lastRow]}>
            <CustomSubheading style={styleV.lastRowText}>
              {`Total`}
            </CustomSubheading>

            <CustomSubheading
              style={styleV.lastRowText}
            >{`${item?.closingBal}`}</CustomSubheading>
          </View>
        )}

        {/* Sales Details */}
        {sales && (
          <View style={[styleV.row, styleV.lastRow]}>
            <CustomSubheading style={styleV.lastRowText}>
              {`Total`}
            </CustomSubheading>

            <CustomSubheading
              style={styleV.lastRowText}
            >{`${item?.customerSaleAmount}`}</CustomSubheading>
          </View>
        )}
      </View>
    );
  };

  //Header
  const renderHeader = () => {
    return (
      <View style={styleV.headerRow}>
        {heading.map((hItem, i) => (
          <CustomSubheading
            key={Math.random() + i}
            style={[
              styleV.heading,
              heading.length - 1 == i && styleV.lastTextObj,
            ]}
          >
            {hItem}
          </CustomSubheading>
        ))}
      </View>
    );
  };

  const buttons = () => {
    return (
      prev && (
        <>
          <View style={{ flexDirection: "row" }}>
            {!next ? (
              <CustomSquareButton
                mode={"contained"}
                title={`Previous`}
                onPress={() => {
                  setData(prev);
                  setNext(false);
                }}
              />
            ) : (
              <TouchableRipple
                onPress={() => {
                  setData(prev);
                  setNext(false);
                }}
              >
                <CustomSubheading style={{ padding: 5 }}>
                  {`PREVIOUS`}
                </CustomSubheading>
              </TouchableRipple>
            )}

            <GapH small={true} />

            {next ? (
              <CustomSquareButton
                mode={"contained"}
                title={`Current`}
                onPress={() => {
                  setData(current);
                  setNext(true);
                }}
              />
            ) : (
              <TouchableRipple
                onPress={() => {
                  setData(current);
                  setNext(true);
                }}
              >
                <CustomSubheading style={{ padding: 5 }}>
                  {`CURRENT`}
                </CustomSubheading>
              </TouchableRipple>
            )}
          </View>
          <GapV small={true} />
        </>
      )
    );
  };

  return (
    <Animated.View
      style={styleV.container}
      // entering={BounceInUp}
      // exiting={FadeOut}
      // layout={Layout.springify()}
    >
      <Surface style={[styleV.box, style]} onLayout={onLayout}>
        <View style={styleV.row}>
          <CustomTitle style={styleV.title}>{title}</CustomTitle>
        </View>

        {buttons()}

        <FlatList
          data={data}
          renderItem={renderItem}
          ListHeaderComponent={renderHeader}
          refreshControl={
            refresh && (
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            )
          }
          keyExtractor={(item, index) => String(index)}
          initialNumToRender={11}
          maxToRenderPerBatch={5}
          contentContainerStyle={styleV.fg}
        />
      </Surface>
      <GapV />
    </Animated.View>
  );
};

const styles = ({ bg, colors }) =>
  StyleSheet.create({
    box: {
      backgroundColor: bg,
      paddingHorizontal: mgHs,
      paddingVertical: pdVs,
      borderRadius: bRs,
      borderTopWidth: bRs,
      borderColor: colors.notification,
      flex: 1,
    },

    container: { flex: 1 },

    row: {
      flexDirection: "row",
      justifyContent: "space-between",
      paddingVertical: pdVs,
      paddingHorizontal: pdHs,
    },

    headerRow: {
      flexDirection: "row",
      justifyContent: "space-between",
      paddingVertical: pdVss,
      borderRadius: bRss,
      paddingHorizontal: pdHs,
    },

    title: {
      textDecorationLine: "underline",
    },

    heading: {
      fontWeight: "bold",
      width: 100,
      textAlign: "left",
    },

    text: { width: windowWidth * 0.25, textAlign: "left" },

    salesText: {},

    lastTextObj: {
      textAlign: "right",
    },

    lastRow: {
      backgroundColor: colors.primary,
    },

    lastRowText: {
      fontWeight: "bold",
      color: onBackgroundDark,
    },

    divider: {
      backgroundColor: colors.text,
      height: StyleSheet.hairlineWidth,
      width: "100%",
      alignSelf: "center",
    },

    fg: { flexGrow: 1 },
  });
