import getDashboardDataReducer from "../../../screens/dashboard/reducers/getDashboardDataReducer";
import dashboardCMReducer from "./dashboardCMReducer";
import dashboardCQReducer from "./dashboardCQReducer";
import dashboardDReducer from "./dashboardDReducer";
import dashboardTRReducer from "./dashboardTRReducer";
import dashboardTPReducer from "./dashboardTPReducer";
import dashboardPMReducer from "./dashboardPMReducer";
import dashboardPQReducer from "./dashboardPQReducer";
import detailsBankBalReducer from "./detailsBankBalReducer.js";
import detailsCQEReducer from "./detailsCQEReducer";
import detailsCQSReducer from "./detailsCQSReducer";
import detailsPQEReducer from "./detailsPQEReducer";
import detailsPQSReducer from "./detailsPQSReducer";

const reducers = {
  getDashboardDataReducer,
  dashboardCMReducer,
  dashboardCQReducer,
  dashboardDReducer,
  dashboardPMReducer,
  dashboardTRReducer,
  dashboardTPReducer,
  dashboardPQReducer,
  detailsBankBalReducer,
  detailsCQEReducer,
  detailsCQSReducer,
  detailsPQEReducer,
  detailsPQSReducer,
};
export default reducers;
