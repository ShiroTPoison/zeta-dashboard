import React, { useState } from "react";
import Animated from "react-native-reanimated";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { View, StyleSheet } from "react-native";
import { useTheme } from "react-native-paper";

import { submitGetDashboardData } from "./actions/actions";
import gloabalStyle, { mgMs, mgVm, zIndexM } from "../../styles/index";
import { GapV } from "../../components/gap";
import { CustomSnackbar } from "../../components/customSnackbar";
import VirtualizedView from "../../components/virtualizedBackedContainer";
import { StickyHeader } from "../../components/stickyHeader";
import { Statistics } from "../../components/statisticBox";
import { items } from "./tempConfig";
import { callApi } from "../../constants/apiCall";
import { parseISO } from "date-fns";

function Dashboard({
  submitLoginReducer,
  submitGetDashboardData,

  getDashboardDataReducer,
  dashboardCMReducer,
  dashboardCQReducer,
  dashboardDReducer,
  dashboardPMReducer,
  dashboardTRReducer,
  dashboardTPReducer,
  dashboardPQReducer,
  companyIdReducer,
  gDateReducer,
  gMonthReducer,
  companyNameReducer,
}) {
  //var
  const { colors } = useTheme();
  const gStyle = gloabalStyle();
  const style = styles(colors);

  const [visibleSnack, setVisibleSnack] = useState(false);
  const [snackMsg] = useState("");
  const [refreshing] = useState(false);
  const [marginToAvoid, setMarginToAvoid] = useState(0);
  const [loading, setLoading] = useState(false);

  function onDismissSnackBar() {
    setVisibleSnack(false);
  }

  function handleSubmitGetDashboardData() {
    callApi({
      data: {
        companyID: companyIdReducer.data,
        userID: submitLoginReducer.data?.users?.id,
        date: parseISO(gDateReducer.data),
        month: parseISO(gMonthReducer.data),
      },
      setLoading: setLoading,
      callApiReducer: getDashboardDataReducer,
      submitCallApi: submitGetDashboardData,
      successFunc: () => {},
      errFunc: () => {},
      catchFunc: () => {},
    });
  }

  const renderItemStatistics = ({
    data,
    prev,
    current,
    title,
    pr,
    payable,
    recievable,
  }) => {
    return (
      <>
        <Statistics
          bg={colors.surface}
          data={data}
          heading={["Category", "Amount"]}
          title={title}
          prev={prev}
          current={current}
          style={gStyle.elevationS}
          pr={pr}
          payable={payable}
          recievable={recievable}
        />
        <GapV />
      </>
    );
  };

  return (
    <Animated.View style={[gStyle.container]}>
      <View style={style.dashboardHeader}>
        <StickyHeader
          profileUrl={submitLoginReducer.data?.user_pp}
          title={submitLoginReducer.data?.session?.employeename}
          subtitle={`Employee Code : ${submitLoginReducer.data?.users?.empCode}`}
          subtitle2={`Company : ${companyNameReducer.data}`}
          setMarginToAvoid={setMarginToAvoid}
        />
      </View>
      <VirtualizedView
        contentContainerStyle={[gStyle.fg]}
        refresh={true}
        refreshing={refreshing}
        onRefresh={async () => {
          try {
            handleSubmitGetDashboardData();
          } catch (e) {
            console.error(e);
          }
        }}
      >
        {loading ? null : (
          <View style={[style.content, { marginTop: marginToAvoid }]}>
            {/* Dashboard Statistics boxes */}

            {/* Daily d */}
            {dashboardDReducer?.data?.dashBoard?.length >= 1
              ? renderItemStatistics({
                  current: dashboardDReducer?.data?.dashBoard,
                  title: "Today",
                })
              : renderItemStatistics({
                  current: items[0].current,
                  title: "Today",
                })}

            {/* Monthly */}
            {dashboardCMReducer?.data?.dashBoard?.length >= 1
              ? renderItemStatistics({
                  current: dashboardCMReducer?.data?.dashBoard,
                  prev:
                    dashboardPMReducer?.data?.dashBoard?.length >= 1 &&
                    dashboardPMReducer?.data?.dashBoard,
                  title: "Month",
                })
              : renderItemStatistics({
                  current: items[0].current,
                  title: "Month",
                })}

            {/* Quarterly */}
            {dashboardCQReducer?.data?.dashBoard?.length >= 1
              ? renderItemStatistics({
                  current: dashboardCQReducer?.data?.dashBoard,
                  prev:
                    dashboardPQReducer?.data?.dashBoard?.length >= 1 &&
                    dashboardPQReducer?.data?.dashBoard,
                  title: "Quarter",
                })
              : renderItemStatistics({
                  current: items[0].current,
                  title: "Quarter",
                })}

            {/* Payable Recievable */}
            {dashboardTRReducer?.data?.dashBoard?.length >= 1
              ? renderItemStatistics({
                  pr: true,
                  payable:
                    dashboardTPReducer?.data?.dashBoard[0]
                      .totalPayableRecievable,
                  recievable:
                    dashboardTRReducer?.data?.dashBoard[0]
                      .totalPayableRecievable,
                  title: "P/R",
                })
              : renderItemStatistics({
                  current: items[0].current,
                  title: "P/R",
                })}
          </View>
        )}
        <GapV />
      </VirtualizedView>

      <CustomSnackbar
        visible={visibleSnack}
        onDismiss={onDismissSnackBar}
        style={gStyle.snackBar}
        textStyle={gStyle.snackText}
        msg={`${snackMsg}`}
      />
    </Animated.View>
  );
}

function mapStateToProps({
  submitLoginReducer,
  getDashboardDataReducer,
  dashboardCMReducer,
  dashboardCQReducer,
  dashboardDReducer,
  dashboardPMReducer,
  dashboardTRReducer,
  dashboardTPReducer,
  dashboardPQReducer,

  companyIdReducer,
  gDateReducer,
  gMonthReducer,
  companyNameReducer,
}) {
  return {
    submitLoginReducer,
    getDashboardDataReducer,
    dashboardCMReducer,
    dashboardCQReducer,
    dashboardDReducer,
    dashboardPMReducer,
    dashboardTRReducer,
    dashboardTPReducer,
    dashboardPQReducer,

    companyIdReducer,
    gDateReducer,
    gMonthReducer,
    companyNameReducer,
  };
}

function mapActionsToProps(dispatch) {
  return bindActionCreators(
    {
      submitGetDashboardData,
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapActionsToProps)(Dashboard);

const styles = (colors) =>
  StyleSheet.create({
    dashboardHeader: {
      minHeight: 80,
      backgroundColor: colors.notification,
      paddingHorizontal: mgMs,
      zIndex: zIndexM,
    },

    content: {
      flex: 1,
      marginTop: mgVm,
      paddingHorizontal: mgMs,
    },
  });
