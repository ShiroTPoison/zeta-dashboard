import * as types from "../constants/constants";
import * as commonTypes from "../../../redux/common/constants/constants";

import axios from "axios";
import { ServerUrl } from "../../../redux/helper/helper";
import { Alert } from "react-native";

import { deviceInfo, versionCode, success } from "../../../constants";

const getConfig = (data, type, getState) => {
  const companyId = getState().companyIdReducer.data;
  const quarter = getState().quarterReducer.data;
  const fyear = getState().fYearReducer.data;

  return {
    method: "post",
    url: `${ServerUrl}Home/GetPosDashBoardData`,
    headers: {
      "Content-Type": "application/json",
    },
    data: {
      vtype: type,
      fyear: fyear,
      quarter: quarter,
      companyID: companyId,
      userID: data?.userID,
      deviceInfo: deviceInfo,
      versionCode: versionCode,
    },
  };
};

export function submitGetDashboardData(data) {
  return async (dispatch, getState) => {
    dispatch({ type: types.GET_DASHBOARD_DATA_ALL_ATTEMPT });

    try {
      const CM = await axios(getConfig(data, "CM", getState)).then(function (
        response
      ) {
        dispatch({
          type: types.GET_DASHBOARD_DATA_CM_SUCCESS,
          payload: response.data,
        });
        // console.log("CM--", response.data);
        return response.data;
      });

      const CQ = await axios(getConfig(data, "CQ", getState)).then(function (
        response
      ) {
        dispatch({
          type: types.GET_DASHBOARD_DATA_CQ_SUCCESS,
          payload: response.data,
        });
        // console.log("CQ--", response.data);
        return response.data;
      });

      const PM = await axios(getConfig(data, "PM", getState)).then(function (
        response
      ) {
        dispatch({
          type: types.GET_DASHBOARD_DATA_PM_SUCCESS,
          payload: response.data,
        });
        // console.log("PM--", response.data);
        return response.data;
      });

      const PQ = await axios(getConfig(data, "PQ", getState)).then(function (
        response
      ) {
        dispatch({
          type: types.GET_DASHBOARD_DATA_PQ_SUCCESS,
          payload: response.data,
        });
        // console.log("PQ--", response.data);
        return response.data;
      });

      const D = await axios(getConfig(data, "D", getState)).then(function (
        response
      ) {
        dispatch({
          type: types.GET_DASHBOARD_DATA_D_SUCCESS,
          payload: response.data,
        });
        // console.log("D--", response.data);
        return response.data;
      });

      const TP = await axios(getConfig(data, "TP", getState)).then(function (
        response
      ) {
        dispatch({
          type: types.GET_DASHBOARD_DATA_TP_SUCCESS,
          payload: response.data,
        });
        // console.log("TP--", response.data);
        return response.data;
      });

      const TR = await axios(getConfig(data, "TR", getState)).then(function (
        response
      ) {
        dispatch({
          type: types.GET_DASHBOARD_DATA_TR_SUCCESS,
          payload: response.data,
        });
        // console.log("TR--", response.data);
        return response.data;
      });

      return (
        CM?.status == success &&
        TP?.status == success &&
        PM?.status == success &&
        D?.status == success &&
        PQ?.status == success &&
        CQ?.status == success &&
        TR?.status == success && { status: "success" }
      );
    } catch (error) {
      console.error("error///", error);
      Alert.alert(
        "Error! Get Dashboard action was unsucessfull. ",
        `${error}\n*Reseting default settings`
      );

      //Reseting Default Settings if API req fail
      dispatch(
        { type: types.GET_DASHBOARD_DATA_ALL_FAIL, payload: error },
        {
          type: commonTypes.COMPANY_ID,
          payload: getState().submitLoginReducer.data?.session?.companyid
            ? getState().submitLoginReducer.data?.session?.companyid
            : null,
        },
        { type: commonTypes.G_DATE, payload: new Date() },
        { type: commonTypes.G_MONTH, payload: new Date() }
      );

      throw new Error(error);
    }
  };
}

export function submitGetDashboardDataDetail(data) {
  let vtype = data.vtype;

  const attemptType =
    vtype == "BankBal"
      ? types.GET_BANKBAL_ATTEMPT
      : vtype == "CQE"
      ? types.GET_CQE_ATTEMPT
      : vtype == "PQE"
      ? types.GET_PQE_ATTEMPT
      : vtype == "CQS"
      ? types.GET_CQS_ATTEMPT
      : vtype == "PQS"
      ? types.GET_PQS_ATTEMPT
      : null;

  const successType =
    vtype == "BankBal"
      ? types.GET_BANKBAL_SUCCESS
      : vtype == "CQE"
      ? types.GET_CQE_SUCCESS
      : vtype == "PQE"
      ? types.GET_PQE_SUCCESS
      : vtype == "CQS"
      ? types.GET_CQS_SUCCESS
      : vtype == "PQS"
      ? types.GET_PQS_SUCCESS
      : null;

  const errorType =
    vtype == "BankBal"
      ? types.GET_BANKBAL_ERROR
      : vtype == "CQE"
      ? types.GET_CQE_ERROR
      : vtype == "PQE"
      ? types.GET_PQE_ERROR
      : vtype == "CQS"
      ? types.GET_CQS_ERROR
      : vtype == "PQS"
      ? types.GET_PQS_ERROR
      : null;

  return async (dispatch, getState) => {
    dispatch({
      type: attemptType,
    });

    return axios(getConfig(data, vtype, getState))
      .then(function (response) {
        dispatch({
          type: successType,
          payload: response.data,
        });
        return response.data;
      })
      .catch(function (error) {
        console.error("error///", error); // Console Log
        Alert.alert(
          "Error! Get Detail action was unsucessfull",
          `${error}\n*Reseting default settings`
        );
        dispatch(
          { type: errorType, payload: error },
          {
            type: commonTypes.COMPANY_ID,
            payload: getState().submitLoginReducer.data?.session?.companyid
              ? getState().submitLoginReducer.data?.session?.companyid
              : null,
          },
          { type: commonTypes.G_DATE, payload: new Date() },
          { type: commonTypes.G_MONTH, payload: new Date() }
        );
        throw new Error(error);
      });
  };
}
