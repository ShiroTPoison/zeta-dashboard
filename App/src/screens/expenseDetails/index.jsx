/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { View } from "react-native";
import { useTheme } from "react-native-paper";
import Animated, { BounceInUp, Layout, FadeOut } from "react-native-reanimated";

//CustomFuncs/components/etc
import { submitGetDashboardDataDetail } from "../dashboard/actions/actions";
import gloabalStyle from "../../styles/index";
import { CustomSnackbar } from "../../components/customSnackbar";
import { callApi } from "../../constants/apiCall";
import VirtualizedView from "../../components/virtualizedBackedContainer";
import { DetailsBox } from "../../components/detailsBox";
import { items } from "../dashboard/tempConfig";
import { parseISO } from "date-fns";
//Local variables for values not re rendering via states

function ExpenseDetails({
  detailsCQEReducer,
  submitLoginReducer,
  detailsPQEReducer,
  companyIdReducer,
  gDateReducer,
  gMonthReducer,
  submitGetDashboardDataDetail,
}) {
  //var
  const { colors } = useTheme();
  const gStyle = gloabalStyle();
  // const style = styles(colors);

  //states
  const [visibleSnack, setVisibleSnack] = useState(false);
  const [snackMsg] = useState("");
  const [refreshing] = useState(false);

  useEffect(() => {
    handleSubmitGetQuarterExpense("CQE");
    handleSubmitGetQuarterExpense("PQE");
  }, []);

  function onDismissSnackBar() {
    setVisibleSnack(false);
  }

  function handleSubmitGetQuarterExpense(type) {
    callApi({
      data: {
        companyID: companyIdReducer.data,
        userID: submitLoginReducer.data?.users?.id,
        date: parseISO(gDateReducer.data),
        month: parseISO(gMonthReducer.data),
        vtype: type,
      },
      setLoading: () => {},
      callApiReducer: type == "CQE" ? detailsCQEReducer : detailsPQEReducer,
      submitCallApi: submitGetDashboardDataDetail,
      successFunc: () => {},
      errFunc: () => {},
      catchFunc: () => {},
    });
  }

  const renderDetailBox = ({ current, prev }) => {
    return (
      <DetailsBox
        title={"Expense Details"}
        current={current}
        prev={prev}
        heading={["Account Code", "Account Name", "Amount"]}
        style={gStyle.elevationS}
        bg={colors.surface}
        expense={true}
      />
    );
  };

  return (
    <Animated.View
      entering={BounceInUp}
      exiting={FadeOut}
      layout={Layout.springify()}
      style={gStyle.container}
    >
      <VirtualizedView
        contentContainerStyle={[gStyle.fg]}
        refresh={true}
        refreshing={refreshing}
        onRefresh={async () => {
          try {
            handleSubmitGetQuarterExpense("CQE");
            handleSubmitGetQuarterExpense("PQE");
          } catch (e) {
            console.error(e);
          }
        }}
      >
        <View style={[gStyle.content]}>
          {detailsCQEReducer.data?.dashBoard?.length >= 1
            ? renderDetailBox({
                current: detailsCQEReducer.data?.dashBoard,
                prev:
                  detailsPQEReducer.data?.dashBoard?.length >= 1 &&
                  detailsPQEReducer.data?.dashBoard,
              })
            : renderDetailBox({ current: items[0].current })}
        </View>
      </VirtualizedView>

      <CustomSnackbar
        visible={visibleSnack}
        onDismiss={onDismissSnackBar}
        style={gStyle.snackBar}
        textStyle={gStyle.snackText}
        msg={`${snackMsg}`}
      />
    </Animated.View>
  );
}

function mapStateToProps({
  detailsCQEReducer,
  submitLoginReducer,
  detailsPQEReducer,
  companyIdReducer,
  gDateReducer,
  gMonthReducer,
}) {
  return {
    detailsCQEReducer,
    submitLoginReducer,
    detailsPQEReducer,
    companyIdReducer,
    gDateReducer,
    gMonthReducer,
  };
}
export default connect(mapStateToProps, { submitGetDashboardDataDetail })(
  ExpenseDetails
);
