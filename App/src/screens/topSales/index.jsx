/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { View } from "react-native";
import { useTheme } from "react-native-paper";
import Animated, { BounceInUp, Layout, FadeOut } from "react-native-reanimated";

//CustomFuncs/components/etc
import { submitGetDashboardDataDetail } from "../dashboard/actions/actions";
import gloabalStyle from "../../styles/index";
import { CustomSnackbar } from "../../components/customSnackbar";
import { callApi } from "../../constants/apiCall";
import VirtualizedView from "../../components/virtualizedBackedContainer";
import { DetailsBox } from "../../components/detailsBox";
import { items } from "../dashboard/tempConfig";
import { parseISO } from "date-fns";
//Local variables for values not re rendering via states

function TopSales({
  detailsPQSReducer,
  submitLoginReducer,
  detailsCQSReducer,
  submitGetDashboardDataDetail,
  companyIdReducer,
  gDateReducer,
  gMonthReducer,
}) {
  //var
  const { colors } = useTheme();
  const gStyle = gloabalStyle();

  //states
  const [visibleSnack, setVisibleSnack] = useState(false);
  const [snackMsg] = useState("");
  const [refreshing] = useState(false);

  useEffect(() => {
    async function effect() {
      await handleSubmitGetQuarterExpense("CQS");
      await handleSubmitGetQuarterExpense("PQS");
    }
    effect();
  }, []);

  function onDismissSnackBar() {
    setVisibleSnack(false);
  }

  async function handleSubmitGetQuarterExpense(type) {
    callApi({
      data: {
        companyID: companyIdReducer.data,
        userID: submitLoginReducer.data?.users?.id,
        date: parseISO(gDateReducer.data),
        month: parseISO(gMonthReducer.data),
        // companyID: 2,
        // userID: 63,
        vtype: type,
      },
      callApiReducer: type == "CQS" ? detailsCQSReducer : detailsPQSReducer,
      submitCallApi: submitGetDashboardDataDetail,

      setLoading: () => {},
      successFunc: () => {},
      errFunc: () => {},
      catchFunc: () => {},
    });
  }

  const renderDetailBox = ({ current, prev }) => {
    return (
      <DetailsBox
        title={"Top Sales"}
        current={current}
        prev={prev}
        heading={["Customer Name", "Sales Amount"]}
        style={gStyle.elevationS}
        bg={colors.surface}
        sales={true}
      />
    );
  };

  return (
    <Animated.View
      entering={BounceInUp}
      exiting={FadeOut}
      layout={Layout.springify()}
      style={gStyle.container}
    >
      <VirtualizedView
        contentContainerStyle={[gStyle.fg]}
        refresh={true}
        refreshing={refreshing}
        onRefresh={async () => {
          try {
            await handleSubmitGetQuarterExpense("CQS");
            await handleSubmitGetQuarterExpense("PQS");
          } catch (e) {
            console.error(e);
          }
        }}
      >
        <View style={gStyle.content}>
          {detailsCQSReducer.data?.dashBoard?.length >= 1
            ? renderDetailBox({
                current: detailsCQSReducer.data?.dashBoard,
                prev:
                  detailsPQSReducer.data?.dashBoard?.length >= 1 &&
                  detailsPQSReducer.data?.dashBoard,
              })
            : renderDetailBox({ current: items[0].current })}
        </View>
      </VirtualizedView>

      <CustomSnackbar
        visible={visibleSnack}
        onDismiss={onDismissSnackBar}
        style={gStyle.snackBar}
        textStyle={gStyle.snackText}
        msg={`${snackMsg}`}
      />
    </Animated.View>
  );
}

function mapStateToProps({
  detailsPQSReducer,
  submitLoginReducer,
  detailsCQSReducer,
  companyIdReducer,
  gDateReducer,
  gMonthReducer,
}) {
  return {
    detailsPQSReducer,
    submitLoginReducer,
    detailsCQSReducer,
    companyIdReducer,
    gDateReducer,
    gMonthReducer,
  };
}
export default connect(mapStateToProps, { submitGetDashboardDataDetail })(
  TopSales
);
