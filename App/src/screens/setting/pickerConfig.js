export var quarterItems = [
  {
    label: "1st Quarter",
    value: 1,
  },
  {
    label: "2nd Quarter",
    value: 2,
  },
  {
    label: "3rd Quarter",
    value: 3,
  },
  {
    label: "4th Quarter",
    value: 4,
  },
];

export var fYearItems = [
  {
    label: "Year 2018-19",
    value: "Year 2018-19",
  },
  {
    label: "Year 2019-20",
    value: "Year 2019-20",
  },
  {
    label: "Year 2020-21",
    value: "Year 2020-21",
  },
  {
    label: "Year 2021-22",
    value: "Year 2021-22",
  },
];
