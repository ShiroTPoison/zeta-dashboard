import * as types from "../constants/constants";
import axios from "axios";
import { ServerUrl } from "../../../redux/helper/helper";
import { Alert } from "react-native";

import { deviceInfo, versionCode } from "../../../constants";

const getConfig = (data, url) => {
  return {
    method: "post",
    url: `${ServerUrl}${url}${data.userID}`,
    // url: `${ServerUrl}${url}63`,
    headers: {
      "Content-Type": "application/json",
    },
    data: {
      deviceInfo: deviceInfo,
      versionCode: versionCode,
    },
  };
};

const getConfigFYear = (id, url) => {
  return {
    method: "get",
    url: `${ServerUrl}${url}${id}`,
    headers: {
      "Content-Type": "application/json",
    },
    data: {
      deviceInfo: deviceInfo,
      versionCode: versionCode,
    },
  };
};

export function submitGetCompanyList(data) {
  return async (dispatch) => {
    dispatch({ type: types.GET_COMPANY_LIST_ATTEMPT });

    return axios(getConfig(data, `Login/GetUserCompany?UserID=`))
      .then(function (response) {
        dispatch({
          type: types.GET_COMPANY_LIST_SUCCESS,
          payload: response.data,
        });
        return response.data;
      })
      .catch(function (error) {
        console.log(getConfig(data, `Login/GetUserCompany`));
        console.error("error///", error); // Console Log
        Alert.alert("Error! getting Company in was unsucessfull", `${error}`);
        dispatch({ type: types.GET_COMPANY_LIST_FAIL, payload: error });
        throw new Error(error);
      });
  };
}

export function submitGetFYearList(data) {
  return async (dispatch) => {
    dispatch({ type: types.GET_F_YEAR_ATTEMPT });

    return axios(
      getConfigFYear(data.companyId, `Login/GetCompanyFinancialYear?CompanyID=`)
    )
      .then(function (response) {
        dispatch({
          type: types.GET_F_YEAR_SUCCESS,
          payload: response.data,
        });
        return response.data;
      })
      .catch(function (error) {
        console.error("error///", error); // Console Log
        Alert.alert(
          "Error! getting Finacnial Year was unsucessfull",
          `${error}`
        );
        dispatch({ type: types.GET_F_YEAR_FAIL, payload: error });
        throw new Error(error);
      });
  };
}
