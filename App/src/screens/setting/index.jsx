/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { StyleSheet, View } from "react-native";
import Animated, { BounceInUp, Layout, FadeOut } from "react-native-reanimated";
import { useTheme } from "react-native-paper";

//redux
import {
  setFYear,
  setQuarter,
  setCompanyId,
  setCompanyName,
} from "../../redux/common/actions/actions";
import { submitGetCompanyList, submitGetFYearList } from "./actions/actions";

//CustomFuncs/components/etc
import gloabalStyle from "../../styles/index";
import { CustomSnackbar } from "../../components/customSnackbar";
import { GapV } from "../../components/gap";
import { CustomSquareButton } from "../../components/buttons";
import VirtualView from "../../components/virtualizedBackedContainer";
import { CustomDropDownPicker } from "../../components/dropDownPicker";
import { CustomParagraph } from "../../components/customText";
import { callApi } from "../../constants/apiCall";
import { quarterItems, fYearItems } from "./pickerConfig";

function Setting({
  fYearReducer,
  quarterReducer,
  companyIdReducer,
  dateFormatReducer,
  companyNameReducer,
  submitLoginReducer,
  getFYearListReducer,
  getCompanyListReducer,
  //

  setFYear,
  setQuarter,
  setCompanyId,
  setCompanyName,
  submitGetFYearList,
  submitGetCompanyList,
}) {
  const { colors } = useTheme();
  const gStyle = gloabalStyle();
  const style = styles(colors);
  const dateFormat = dateFormatReducer.data;

  //states
  const [ready, setReady] = useState(false);
  const [snackMsg, setSnackMsg] = useState("");
  const [loading, setLoading] = useState(false);
  const [visibleSnack, setVisibleSnack] = useState(false);
  const [fYearReady, setFYearReady] = useState(false);

  const [selectedQuarter, setSelectedQuarter] = useState(quarterReducer.data);

  const [selectedFYear, setSelectedFYear] = useState(fYearReducer.data);
  const [fYearList, setFYearList] = useState([]);

  const [selectedCompany, setSelectedCompany] = useState(companyIdReducer.data);
  const [companyList, setCompanyList] = useState([]);

  const [selectedCompanyName, setSelectedCompanyName] = useState(
    companyNameReducer.data
  );

  useEffect(() => {
    async function effect() {
      handleGetCompanyList();
    }

    effect();
    return () => {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    async function effect() {
      handleGetFYearList();
    }
    effect();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedCompany]);

  function onDismissSnackBar() {
    setVisibleSnack(false);
  }

  function showSnack(msg) {
    setSnackMsg(msg);
    setVisibleSnack(true);
  }

  function onSelectItemCompany(item) {
    setCompanyName(item?.label);
    setCompanyId(item?.value);
    setSelectedCompanyName(item?.label);
    //redux
  }

  //Handle Save press
  function handleSave() {
    if (
      selectedCompany === null ||
      selectedCompany === undefined ||
      !selectedCompany
    ) {
      showSnack("Please Select Company");
    } else {
      setLoading(true);

      if (selectedCompany != null) setCompanyId(selectedCompany);
      if (selectedCompanyName != "" || selectedCompanyName != null) {
        setCompanyName(selectedCompanyName);
      }
      if (selectedQuarter !== "" || selectedQuarter !== null) {
        setQuarter(selectedQuarter);
      }
      if (selectedFYear !== "" || selectedFYear !== null) {
        setFYear(selectedFYear);
      }

      showSnack("Updated Successfully");
      setLoading(false);
    }
  }

  //Get COMPANY list from Api
  function handleGetCompanyList() {
    callApi({
      data: {
        userID: submitLoginReducer.data?.users?.id,
      },
      callApiReducer: getCompanyListReducer,
      submitCallApi: submitGetCompanyList,

      successFunc: (res) => {
        let arr = [];
        res?.userCompany.map((item) => {
          arr.push({ value: item?.companyID, label: item?.companyName });
        });
        setCompanyList(arr);
        setReady(true);
      },

      setLoading: () => {},
      errFunc: () => {},
      catchFunc: () => {},
    });
  }

  //Get FYEAR list from Api
  function handleGetFYearList() {
    setFYearReady(false);

    callApi({
      data: {
        companyId: selectedCompany,
      },
      callApiReducer: fYearReducer,
      submitCallApi: submitGetFYearList,

      successFunc: (res) => {
        let arr = [];
        res?.companyFyears.map((item) => {
          arr.push({ value: item?.financialYearID, label: item?.fyearName });
        });
        setFYearList(arr);
        setFYearReady(true);
      },

      setLoading: () => {},
      errFunc: () => {},
      catchFunc: () => {},
    });
  }

  return (
    <Animated.View
      entering={BounceInUp}
      exiting={FadeOut}
      layout={Layout.springify()}
      style={gStyle.container}
    >
      <VirtualView contentContainerStyle={[gStyle.fg]}>
        <View style={gStyle.content}>
          {/* Content */}

          {/* companyPicker */}
          <CustomDropDownPicker
            value={selectedCompany}
            items={ready ? companyList : []}
            setValue={setSelectedCompany}
            setItems={setCompanyList}
            onSelectItem={onSelectItemCompany}
            searchable={true}
            modalTitle={`Select Company`}
            placeholder={
              companyList.length === 0 ? "No data" : "Select Company"
            }
            title="Select Company"
          />
          <GapV />

          {/* fYearPicker */}
          <CustomDropDownPicker
            value={selectedFYear}
            items={fYearReady ? fYearList : []}
            setValue={setSelectedFYear}
            modalTitle={`Select Finacncial Year`}
            placeholder={fYearList.length === 0 ? "No Data" : "Select F - year"}
            title="Select Finacnial Year"
          />
          <GapV />

          {/* quarterPicker */}
          <CustomDropDownPicker
            value={selectedQuarter}
            items={quarterItems}
            setValue={setSelectedQuarter}
            modalTitle={`Select Quarter`}
            placeholder="Select Quarter"
            title="Select Quarter"
          />
          <GapV />

          {/* Save */}
          <CustomSquareButton
            title={`Save`}
            onPress={handleSave}
            loading={loading}
          />
          <GapV />

          <CustomParagraph>{`**Note: \nYou will have to refresh the pages after changing to load new data.`}</CustomParagraph>
        </View>
      </VirtualView>

      <CustomSnackbar
        visible={visibleSnack}
        onDismiss={onDismissSnackBar}
        style={gStyle.snackBar}
        textStyle={gStyle.snackText}
        msg={`${snackMsg}`}
      />
    </Animated.View>
  );
}

function mapStateToProps({
  fYearReducer,
  quarterReducer,
  companyIdReducer,
  dateFormatReducer,
  submitLoginReducer,
  companyNameReducer,
  getFYearListReducer,
  getCompanyListReducer,
}) {
  return {
    fYearReducer,
    quarterReducer,
    companyIdReducer,
    dateFormatReducer,
    companyNameReducer,
    submitLoginReducer,
    getFYearListReducer,
    getCompanyListReducer,
  };
}

function mapActionsToProps(dispatch) {
  return bindActionCreators(
    {
      setFYear,
      setQuarter,
      setCompanyId,
      setCompanyName,
      submitGetFYearList,
      submitGetCompanyList,
    },
    dispatch
  );
}

export default connect(mapStateToProps, mapActionsToProps)(Setting);

const styles = (colors) => StyleSheet.create({});
