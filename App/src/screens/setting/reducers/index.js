import getCompanyListReducer from "./getCompanyListReducer";
import getFYearListReducer from "./getFYearListReducer";

const reducers = {
  getCompanyListReducer,
  getFYearListReducer,
};
export default reducers;
