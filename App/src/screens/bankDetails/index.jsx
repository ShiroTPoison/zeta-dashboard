/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { View } from "react-native";
import { useTheme } from "react-native-paper";
import Animated, { BounceInUp, Layout, FadeOut } from "react-native-reanimated";

//CustomFuncs/components/etc
import { submitGetDashboardDataDetail } from "../dashboard/actions/actions";
import gloabalStyle from "../../styles/index";
import { CustomSnackbar } from "../../components/customSnackbar";
import { callApi } from "../../constants/apiCall";
import VirtualizedView from "../../components/virtualizedBackedContainer";
import { DetailsBox } from "../../components/detailsBox";
import { items } from "../dashboard/tempConfig";
import { parseISO } from "date-fns";
//Local variables for values not re rendering via states

function BankDetails({
  detailsBankBalReducer,
  companyIdReducer,
  gDateReducer,
  gMonthReducer,
  submitLoginReducer,
  submitGetDashboardDataDetail,
}) {
  //var
  const { colors } = useTheme();
  const gStyle = gloabalStyle();
  // const style = styles(colors);

  //states
  const [visibleSnack, setVisibleSnack] = useState(false);
  const [snackMsg] = useState("");
  const [refreshing] = useState(false);

  useEffect(() => {
    handleSubmitGetBankDetails();
  }, []);

  // function navigate() {
  //   navigation.navigate("tabNav");
  // }

  function onDismissSnackBar() {
    setVisibleSnack(false);
  }

  function handleSubmitGetBankDetails() {
    callApi({
      data: {
        companyID: companyIdReducer.data,
        userID: submitLoginReducer.data?.users?.id,
        date: parseISO(gDateReducer.data),
        month: parseISO(gMonthReducer.data),
        // companyID: 2,
        // userID: 63,
        vtype: "BankBal",
      },

      setLoading: () => {},

      callApiReducer: detailsBankBalReducer,

      submitCallApi: submitGetDashboardDataDetail,

      successFunc: () => {
        // console.log(detailsBankBalReducer);
      },

      errFunc: () => {},

      catchFunc: () => {},
    });
  }

  const renderDetailBox = ({ data }) => {
    return (
      <DetailsBox
        title={"Bank Details"}
        current={data}
        heading={["Bank Code", "Bank Name", "Amount"]}
        style={gStyle.elevationS}
        bg={colors.surface}
        bank={true}
      />
    );
  };

  return (
    <Animated.View
      entering={BounceInUp}
      exiting={FadeOut}
      layout={Layout.springify()}
      style={gStyle.container}
    >
      <VirtualizedView
        contentContainerStyle={[gStyle.fg]}
        refresh={true}
        refreshing={refreshing}
        onRefresh={async () => {
          handleSubmitGetBankDetails();
        }}
      >
        <View style={[gStyle.content]}>
          {detailsBankBalReducer.data?.dashBoard?.length >= 1
            ? renderDetailBox({ data: detailsBankBalReducer.data?.dashBoard })
            : renderDetailBox({ data: items[0].current })}
        </View>
      </VirtualizedView>

      <CustomSnackbar
        visible={visibleSnack}
        onDismiss={onDismissSnackBar}
        style={gStyle.snackBar}
        textStyle={gStyle.snackText}
        msg={`${snackMsg}`}
      />
    </Animated.View>
  );
}

function mapStateToProps({
  detailsBankBalReducer,
  submitLoginReducer,
  companyIdReducer,
  gDateReducer,
  gMonthReducer,
}) {
  return {
    detailsBankBalReducer,
    submitLoginReducer,
    companyIdReducer,
    gDateReducer,
    gMonthReducer,
  };
}
export default connect(mapStateToProps, { submitGetDashboardDataDetail })(
  BankDetails
);
